defmodule Z do
  @moduledoc """
  """

  @array [1, 4, 5, 2, 3, 9, 8, 11, 0]

  def group_by(x, acc) do
    case acc do
      [] -> [[x]]
      _ ->
        [[h | _] = head | tail] = acc
        cond do
          x == h + 1 -> [[x, List.last head] | tail]
          true -> [[x] | acc]
        end
    end
  end

  def to_string(x, acc) do
    case acc do
      "" -> case x do
              [a] -> "#{a}" <> acc
              [a, b] -> "#{b}-#{a}" <> acc
            end
      _ ->
        case x do
          [a] -> "#{a}," <> acc
          [a, b] -> "#{b}-#{a}," <> acc
        end
    end
  end

  def alter_array do
    @array
    |> Enum.sort
    |> Enum.reduce([], &Z.group_by/2)
    |> Enum.reduce("", &Z.to_string/2)
  end
end

IO.inspect Z.alter_array(), charlists: :as_lists